﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vermelho.Response
{
    public class UsuarioResponse
    {
        public Int32 IdUsuario { get; set; }
        public string Nome { get; set; }
        public string Username { get; set; }
        public Boolean Facebook { get; set; }
    }
}