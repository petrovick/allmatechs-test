﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vermelho.Generic;
using Vermelho.Helper;
using Vermelho.Response;

namespace Vermelho.Controllers
{
    [ActionFilterSecurity]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            int idUsuario = int.Parse(Session["IdUsuario"].ToString());

            var url = ConstantHelper.ServerUrlAutenticacao() + "api/Usuario/BuscarUsuario?idUsuario=" + idUsuario;
            var response = new GenericRequest<Int32, UsuarioResponse>().MakeGetRequest(url);

            ViewBag.NomeUsuario = response.Nome;
            return View();
        }
    }
}
