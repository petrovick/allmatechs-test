﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace Vermelho.Generic
{
    public class GenericRequest<TRequest, TResponse>
    {
        public TResponse MakePostRequest(TRequest request, string url)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.PostAsync(url, new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json")).Result;
            var conteudoRetornadoDoServidorAutenticacao = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<TResponse>(conteudoRetornadoDoServidorAutenticacao);
        }
        public TResponse MakeGetRequest(string url)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync(url).Result;
            var conteudoRetornadoDoServidorAutenticacao = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<TResponse>(conteudoRetornadoDoServidorAutenticacao);
        }
    }
}