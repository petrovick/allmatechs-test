﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vermelho.Helper
{
    public class ConstantHelper
    {
        public static string ServerUrlAutenticacao()
        {
            return System.Configuration.ConfigurationManager.AppSettings["ServerUrlAutenticacao"];
        }
        public static string ServerUrl()
        {
            return System.Configuration.ConfigurationManager.AppSettings["ServerUrl"];
        }

    }
}