﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Allmatech.Business.BusinessObject
{
    [Table("Modulo")]
    public class Modulo
    {
        [Key]
        public Int32 IdModulo { get; set; }
        [Column("Nome")]
        public string Nome { get; set; }
        [Column("Descricao")]
        public string Descricao { get; set; }
        [Column("Url")]
        public string Url { get; set; }
        [Column("UrlImagem")]
        public string UrlImagem { get; set; }
    }
}