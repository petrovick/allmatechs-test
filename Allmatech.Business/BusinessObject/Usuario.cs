﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Allmatech.Business.BusinessObject
{
    [Table("Usuario")]
    public class Usuario
    {
        [Key]
        public Int32 IdUsuario { get; set; }
        [Column("Nome")]
        public string Nome { get; set; }
        [Column("Username")]
        public string Username { get; set; }
        [Column("Senha")]
        public string Senha { get; set; }
        [Column("Facebook")]
        public Boolean Facebook { get; set; }
        [Column("Token")]
        public string Token { get; set; }
        [Column("RedeSocialId")]
        public string RedeSocialId { get; set; }
        

        private ICollection<UsuarioModulo> UsuarioModulos;
    }
}