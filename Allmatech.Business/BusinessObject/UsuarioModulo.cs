﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Allmatech.Business.BusinessObject
{
    [Table("UsuarioModulo")]
    public class UsuarioModulo
    {
        [Key, Column(Order = 0)]
        public Int32 IdUsuario { get; set; }

        [Key, Column(Order = 1)]
        public Int32 IdModulo { get; set; }

        [ForeignKey("IdUsuario")]
        public virtual Usuario Usuario { get; set; }

        [ForeignKey("IdModulo")]
        public virtual Modulo Modulo { get; set; }
    }
}