﻿using Allmatech.Application.ApplicationInterface;
using Allmatech.Application.Factory;
using Allmatech.Servico.Helper;
using Allmatech.Servico.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Allmatech.Servico.Controllers
{
    [AuthorizeFilter]
    [RoutePrefix("api/Modulo")]
    public class ModuloController : ApiController
    {
        private readonly IUsuarioModuloApplication _UsuarioModuloApplication =
            ApplicationFactory.getInstance().getIUsuarioModuloApplication();

        [HttpGet]
        [AuthorizeFilter]
        public IHttpActionResult ListarModulosDoUsuario(int idUsuario)
        {
            ModuloViewModel model = new ModuloViewModel();
            model.Modulos = _UsuarioModuloApplication.Find(x => x.IdUsuario == idUsuario).Select(x => x.Modulo).ToList();
            //return null;
            return Json(model);
        }
    }
}