﻿using Allmatech.Application.ApplicationInterface;
using Allmatech.Application.Factory;
using Allmatech.Business.BusinessObject;
using Allmatech.Servico.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace Allmatech.Servico.Controllers
{
    public class LoginController : ApiController
    {
        private IUsuarioApplication _UsuarioApplication = ApplicationFactory.getInstance().getIUsuarioApplication();

        public HttpResponseMessage Login(Usuario request)
        {
            var resp = new HttpResponseMessage();
            string erro = "";
            try
            {
                var usuario = _UsuarioApplication.Find(x => x.Username.Equals(request.Username)).FirstOrDefault();
                if (usuario != null)
                {
                    if (string.IsNullOrEmpty(usuario.Senha))
                    {
                        erro += "Usuário ou senha incorretos!";
                    }
                    else if (usuario.Senha.Equals(Criptografia.sha256(request.Senha)))
                    {
                        var cookie = new CookieHeaderValue("sessionAllmatech", new GerarTokenCookie().GerarToken(usuario.IdUsuario, request.Senha));
                        cookie.Expires = DateTimeOffset.Now.AddYears(15);
                        cookie.Domain = Request.RequestUri.Host;
                        //cookie.Path = "/";

                        resp.Headers.AddCookies(new CookieHeaderValue[] { cookie });
                        return resp;
                    }
                    erro += "Usuário ou senha incorretos!";
                }
                else
                {
                    erro += "Usuário ou senha incorretos!";
                }
            }
            catch (Exception ex)
            {
                erro = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
            }
            resp = Request.CreateResponse(HttpStatusCode.BadRequest, erro);
            return resp;
        }
    }
}