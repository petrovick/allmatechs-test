﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allmatech.Servico.Helper
{
    public class UrlHelper
    {
        public static string IdModulo()
        {
            return System.Configuration.ConfigurationManager.AppSettings["IdModulo"];
        }

        public static string ServerUrlAutenticacao()
        {
            return System.Configuration.ConfigurationManager.AppSettings["ServerUrlAutenticacao"];
        }
    }
}