﻿using Allmatech.Servico.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;

namespace Allmatech.Servico.Helper
{

    public class AuthorizeFilter : ActionFilterAttribute
    {

        public override void OnActionExecuted(HttpActionExecutedContext filterContext)
        {
            var cabecalhoAuthorization = filterContext.Request.Headers.Authorization;
            string tokenRecebido = cabecalhoAuthorization.Parameter;
            string erro = new ValidarToken().ValidandoToken(tokenRecebido);
            //var idUsuario = filterContext.HttpContext.Session["IdUsuario"] + "";
            if (!string.IsNullOrEmpty(erro))
            {
                filterContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
            }
        }
    }

}