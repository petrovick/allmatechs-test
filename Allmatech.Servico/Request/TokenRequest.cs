﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allmatech.Servico.Request
{
    public class TokenRequest
    {
        public int IdModulo{ get; set; }
        public string DataHora { get; set; }
    }
}