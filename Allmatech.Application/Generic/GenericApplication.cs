﻿using Allmatech.Model.Factory;
using Allmatech.Model.GenericRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;

namespace Allmatech.Application.Generic
{
    public class GenericApplication<TEntity, TKey> : IDisposable, IGenericApplication<TEntity, TKey>
        where TEntity : class
    {
        #region Properties

        protected IRepository<TEntity> Repository;

        #endregion Properties

        #region Constructors

        public GenericApplication()
        {
            Repository = RepositoryFactory<TEntity>.GetInstance().GetRepository();
        }

        #endregion

        #region SaveMethods

        /// <summary>
        ///     <para>Responsável para inserir o objeto passado como parâmetro no contexto</para>
        ///     <para>Deverá-se utilizar o método SaveChanges() para confirmar as alterações no Banco de Dados</para>
        /// </summary>
        public string Save(TEntity entity)
        {
            var erros = "";

            try
            {
                Repository.Save(entity);
            }

            catch (Exception ex)
            {
                erros = "Ocorreu um erro ao inserir. " + ex.Message;
            }

            return erros;
        }

        #endregion

        #region UpdateMethods

        /// <summary>
        ///     <para>Responsável por alterar o estado do objeto passado como parâmetro para alterado</para>
        ///     <para>Deverá-se utilizar o método SaveChanges() para confirmar as alterações no Banco de Dados</para>
        /// </summary>
        public string Update(TEntity entity)
        {
            var erros = "";

            try
            {
                Repository.Update(entity);
            }

            catch (Exception ex)
            {
                erros = "Ocorreu um erro ao atualizar. " + ex.Message;
            }

            return erros;
        }

        #endregion

        #region DbContext

        /// <summary>
        ///     <para>Utilizado para confirmar as alterações feitas no contexto</para>
        ///     <para>Utilize para persistir as alterações no banco de dados</para>
        /// </summary>
        public string SaveChanges()
        {
            var erros = "";

            try
            {
                Repository.SaveChanges();
            }

            catch (DbEntityValidationException e)
            {
                var errorMessage = "";

                foreach (var eve in e.EntityValidationErrors)
                {
                    errorMessage +=
                        string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);

                    foreach (var ve in eve.ValidationErrors)
                        errorMessage += string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName,
                            ve.ErrorMessage);
                }

                return errorMessage;
            }

            catch (Exception ex)
            {
                erros = "Ocorreu um erro ao salvar alterações. " + ex.Message;
                if (ex.InnerException != null)
                    erros += " | " + ex.InnerException;
            }

            return erros;
        }

        /// <summary>
        ///     <para>Utilizado para obter a conexão que essa application possui com o banco de dados</para>
        ///     <para>Útil se é necessário alterar objetos de contextos diferentes</para>
        ///     <para>Deve ser utilizado em conjunto com o método SetContext</para>
        /// </summary>
        public DbContext GetContext()
        {
            return Repository.GetContext();
        }

        /// <summary>
        ///     <para>Utilizado para alterar a conexão que essa application possui com o banco de dados</para>
        ///     <para>Útil se é necessário alterar objetos de contextos diferentes</para>
        ///     <para>Deve ser utilizado em conjunto com o método SetContext</para>
        /// </summary>
        public void SetContext(DbContext context)
        {
            Repository.SetContext(context);
        }

        #endregion

        #region DeleteMethods

        /// <summary>
        ///     <para>Responsável por alterar o estado do objeto identificado pelo Id passado como parâmetro para Excluído</para>
        ///     <para>Deverá-se utilizar o método SaveChanges() para confirmar as alterações no Banco de Dados</para>
        /// </summary>
        public string Delete(TKey id)
        {
            var erros = "";

            try
            {
                var entity = Single(id);
                Repository.Delete(entity);
            }

            catch (Exception ex)
            {
                erros = "Ocorreu um erro ao excluir. " + ex.Message;
            }

            return erros;
        }

        /// <summary>
        ///     <para>Responsável por alterar o estado do objeto passado como parâmetro para excluído</para>
        ///     <para>Deverá-se utilizar o método SaveChanges() para confirmar as alterações no Banco de Dados</para>
        /// </summary>
        public string Delete(TEntity entity)
        {
            var erros = "";

            try
            {
                Repository.Delete(entity);
            }

            catch (Exception ex)
            {
                erros = "Ocorreu um erro ao excluir. " + ex.Message;
            }

            return erros;
        }

        /// <summary>
        ///     <para>
        ///         Responsável por alterar o estado do objeto identificado através do predicado passado como parâmetro para
        ///         excluído
        ///     </para>
        ///     <para>Deverá-se utilizar o método SaveChanges() para confirmar as alterações no Banco de Dados</para>
        /// </summary>
        public string Delete(Func<TEntity, bool> predicate)
        {
            var erros = "";

            try
            {
                Repository.Delete(predicate);
            }

            catch (Exception ex)
            {
                erros = "Ocorreu um erro ao excluir. " + ex.Message;
            }

            return erros;
        }

        /// <summary>
        ///     <para>Responsável por alterar o estado dos objetos passados como parâmetro para excluídos</para>
        ///     <para>Deverá-se utilizar o método SaveChanges() para confirmar as alterações no Banco de Dados</para>
        /// </summary>
        public string DeleteAll(IEnumerable<TEntity> entities)
        {
            var erros = "";

            try
            {
                Repository.DeleteAll(entities);
            }

            catch (Exception ex)
            {
                erros = "Ocorreu um erro" + ex.Message;
            }

            return erros;
        }

        #endregion

        #region ReadMethods

        /// <summary>
        ///     <para>Responsável por retornar todos os registros do banco de dados</para>
        /// </summary>
        public IEnumerable<TEntity> GetAll()
        {
            IEnumerable<TEntity> entity = Repository.GetAll();
            return entity;
        }

        /// <summary>
        ///     <para>Retorna todos os registros do banco de dados com o [Include] para as classes passadas como parâmetro</para>
        /// </summary>
        public IEnumerable<TEntity> GetAll(params string[] includes)
        {
            var entity = Repository.GetAll(includes);
            return entity;
        }

        /// <summary>
        ///     <para>Responsável por retornar todos os registros do banco de dados</para>
        /// </summary>
        public IQueryable<TEntity> GetAllAsQueryAble()
        {
            var entity = Repository.GetAll();
            return entity;
        }

        /// <summary>
        ///     <para>Retorna todos os registros do banco de dados com o [Include] para as classes passadas como parâmetro</para>
        /// </summary>
        public IQueryable<TEntity> GetAllAsQueryAble(params string[] includes)
        {
            var entity = Repository.GetAllAsQueryAble(includes);
            return entity;
        }

        /// <summary>
        ///     <para>Retorna os registros do banco de dados que atendem ao predicado passado como parâmetro</para>
        ///     <para>retorna os registros com o [Include] para as classes passadas como parâmetro</para>
        /// </summary>
        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> where, params string[] includes)
        {
            var entity = Repository.Find(where, includes);
            return entity;
        }

        /// <summary>
        ///     <para>Retorna os registros do banco de dados que atendem ao predicado passado como parâmetro</para>
        /// </summary>
        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> where)
        {
            if (where != null)
            {
                var entity = Repository.Find(where);
                return entity;
            }

            return Enumerable.Empty<TEntity>();
        }

        /// <summary>
        ///     <para>Retorna um registro a partir do Id passado como parâmetro</para>
        /// </summary>
        public TEntity Single(TKey id)
        {
            var entity = Repository.Single(id);
            return entity;
        }

        #endregion

        #region Repository

        /// <summary>
        ///     <para>Responsável por fechar o contexto da Application</para>
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            Repository.Dispose();
        }

        #endregion
    }
}