﻿using Allmatech.Application.Generic;
using Allmatech.Business.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allmatech.Application.ApplicationInterface
{
    public interface IUsuarioApplication : IGenericApplication<Usuario, Int32>
    {
        string Salvar(Usuario usuario);
    }
}