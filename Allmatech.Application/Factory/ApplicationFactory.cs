﻿using Allmatech.Application.ApplicationImplementation;
using Allmatech.Application.ApplicationInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allmatech.Application.Factory
{
    public class ApplicationFactory
    {
        private static ApplicationFactory applicationFactory;

        private ApplicationFactory() { }

        public static ApplicationFactory getInstance()
        {
            if (applicationFactory == null)
                applicationFactory = new ApplicationFactory();
            return applicationFactory;
        }

        public IUsuarioApplication getIUsuarioApplication()
        {
            return new UsuarioApplication();
        }

        public IModuloApplication getIModuloApplication()
        {
            return new ModuloApplication();

        }

        public IUsuarioModuloApplication getIUsuarioModuloApplication()
        {
            return new UsuarioModuloApplication();

        }

    }
}