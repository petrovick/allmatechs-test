﻿using Allmatech.Application.ApplicationInterface;
using Allmatech.Application.Generic;
using Allmatech.Business.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allmatech.Application.ApplicationImplementation
{
    public class UsuarioApplication : GenericApplication<Usuario, int>, IUsuarioApplication
    {
        public string Salvar(Usuario usuario)
        {
            string erro = "";
            Usuario usuarioExiste = this.Find(x => (!string.IsNullOrEmpty(x.RedeSocialId) 
                && x.RedeSocialId.Equals(usuario.RedeSocialId)) || x.IdUsuario == usuario.IdUsuario).FirstOrDefault();

            if(usuarioExiste != null && usuarioExiste.IdUsuario > 0)
            {
                usuarioExiste.Facebook = usuario.Facebook;
                usuarioExiste.Nome = usuario.Nome;
                usuarioExiste.Senha = usuario.Senha;
                usuarioExiste.Token = usuario.Token;
                erro += this.Update(usuarioExiste);
            }
            else
            {
                erro += this.Save(usuario);
            }
            erro += this.SaveChanges();
            return erro;
        }

    }
}