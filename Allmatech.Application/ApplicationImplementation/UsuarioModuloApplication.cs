﻿using Allmatech.Application.ApplicationInterface;
using Allmatech.Application.Generic;
using Allmatech.Business.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allmatech.Application.ApplicationImplementation
{
    public class UsuarioModuloApplication : GenericApplication<UsuarioModulo, int>, IUsuarioModuloApplication
    {
        
    }
}