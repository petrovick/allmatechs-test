﻿using Allmatech.Application.ApplicationInterface;
using Allmatech.Application.Factory;
using Allmatech.Business.BusinessObject;
using Allmatech.Service.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;

namespace Allmatech.Service.Controllers
{
    public class UsuarioController : ApiController
    {
        private readonly IUsuarioApplication _UsuarioApplication = ApplicationFactory.getInstance().getIUsuarioApplication();
        [HttpGet]
        public JsonResult<UsuarioResponse> BuscarUsuario(int idUsuario)
        {
            var usuario = _UsuarioApplication.Single(idUsuario);
            usuario.Senha = "";
            usuario.Token = "";
            var usuarioResponse = new UsuarioResponse()
            {
                IdUsuario = usuario.IdUsuario,
                Nome = usuario.Nome,
                Username = usuario.Username,
                Facebook = usuario.Facebook
            };
            return Json(usuarioResponse);
        }
        [HttpGet]
        public JsonResult<UsuarioResponse> BuscarUsuarioPorRedeSocial(string redeSocialId)
        {
            var usuarioResponse = new UsuarioResponse();
            try
            {
                var usuario = _UsuarioApplication.Find(x => x.RedeSocialId == redeSocialId).FirstOrDefault();
                if (usuario == null)
                    throw new Exception("Não existe usuário com este identificador");
                usuario.Senha = "";
                usuario.Token = "";
                usuarioResponse = new UsuarioResponse()
                {
                    IdUsuario = usuario.IdUsuario,
                    Nome = usuario.Nome,
                    Username = usuario.Username,
                    Facebook = usuario.Facebook
                };
                usuarioResponse.Message = "";
                usuarioResponse.Status = 200;
            }
            catch(Exception ex)
            {
                usuarioResponse.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                if (usuarioResponse.Message == "Não existe usuário com este identificador")
                    usuarioResponse.Status = 404;
                else
                    usuarioResponse.Status = 500;
            }
            return Json(usuarioResponse);
        }


        [HttpPost]
        public JsonResult<UsuarioResponse> SalvarUsuario(Usuario usuario)
        {
            var usuarioResponse = new UsuarioResponse();
            try
            {
                string erro = _UsuarioApplication.Salvar(usuario);
                if (!string.IsNullOrEmpty(erro))
                    throw new Exception(erro);
                usuarioResponse.IdUsuario = usuario.IdUsuario;
                usuarioResponse.Nome = usuario.Nome;
                usuarioResponse.Username = usuario.Username;
                usuarioResponse.Facebook = usuario.Facebook;
                usuarioResponse.Message = "";
                usuarioResponse.Status = 200;
            }
            catch(Exception ex)
            {
                usuarioResponse.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                usuarioResponse.Status = 500;
            }
            return Json(usuarioResponse);
        }

    }
}