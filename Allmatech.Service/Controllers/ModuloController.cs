﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Allmatech.Service.Helper;
using Allmatech.Application.ApplicationInterface;
using Allmatech.Application.Factory;
using Allmatech.Service.Models;

namespace Allmatech.Service.Controllers
{
    [AuthorizeFilter]
    [RoutePrefix("api/Modulo")]
    public class ModuloController : ApiController
    {
        private readonly IUsuarioModuloApplication _UsuarioModuloApplication =
            ApplicationFactory.getInstance().getIUsuarioModuloApplication();

        [HttpGet]
        [AuthorizeFilter]
        public IHttpActionResult ListarModulosDoUsuario(int idUsuario)
        {
            ModuloViewModel model = new ModuloViewModel();
            model.Modulos = _UsuarioModuloApplication.Find(x => x.IdUsuario == idUsuario).Select(x => x.Modulo).ToList();
            
            return Json(model);
        }
    }
}