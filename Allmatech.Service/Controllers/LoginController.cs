﻿using Allmatech.Application.ApplicationInterface;
using Allmatech.Application.Factory;
using Allmatech.Business.BusinessObject;
using Allmatech.Service.Helper;
using Allmatech.Service.Response;
using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;

namespace Allmatech.Service.Controllers
{
    public class LoginController : ApiController
    {
        private IUsuarioApplication _UsuarioApplication = ApplicationFactory.getInstance().getIUsuarioApplication();

        public JsonResult<LoginResponse> Login(Usuario request)
        {
            var resp = new LoginResponse();
            string erro = "";
            try
            {
                var usuario = _UsuarioApplication.Find(x => x.Username.Equals(request.Username)).FirstOrDefault();
                if (usuario != null)
                {
                    if (string.IsNullOrEmpty(usuario.Senha))
                    {
                        erro += "Usuário ou senha incorretos!";
                    }
                    else if (usuario.Senha.Equals(Criptografia.sha256(request.Senha)))
                    {
                        resp.AllmatechCookie = new GerarTokenCookie().GerarToken(usuario.IdUsuario, request.Senha);
                        resp.IdUsuario = usuario.IdUsuario;

                        resp.Message = "Login realizado com sucesso.";
                        resp.Status = 200;
                        return Json(resp);
                    }
                    erro += "Usuário ou senha incorretos!";
                }
                else
                {
                    erro += "Usuário ou senha incorretos!";
                }
            }
            catch (Exception ex)
            {
                erro = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
            }
            resp.Status = 500;
            resp.Message = erro;
            return Json(resp);
        }
        
    }
}