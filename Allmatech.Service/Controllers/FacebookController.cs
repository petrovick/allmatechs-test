﻿using Allmatech.Application.ApplicationInterface;
using Allmatech.Application.Factory;
using Allmatech.Business.BusinessObject;
using Allmatech.Service.Helper;
using Allmatech.Service.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;

namespace Allmatech.Service.Controllers
{
    public class FacebookController : ApiController
    {
        private IUsuarioApplication _UsuarioApplication = ApplicationFactory.getInstance().getIUsuarioApplication();

        public JsonResult<LoginResponse> Facebook(Usuario request)
        {
            var resp = new LoginResponse();
            string erro = "";
            try
            {
                var usuario = _UsuarioApplication.Find(x => x.RedeSocialId.Equals(request.RedeSocialId)).FirstOrDefault();
                if (usuario != null)
                {
                    if (string.IsNullOrEmpty(usuario.Token))
                    {
                        erro += "Usuário ou senha incorretos!";
                    }
                    else if (usuario.Token.Equals(request.Token))
                    {
                        resp.AllmatechCookie = new GerarTokenCookie().GerarToken(usuario.IdUsuario, request.Token);
                        resp.IdUsuario = usuario.IdUsuario;

                        resp.Message = "Login realizado com sucesso.";
                        resp.Status = 200;
                        return Json(resp);
                    }
                    erro += "Usuário ou senha incorretos!";
                }
                else
                {
                    erro += "Usuário ou senha incorretos!";
                }
            }
            catch (Exception ex)
            {
                erro = ex.InnerException == null ? ex.Message : ex.InnerException.Message + " " + ex.Message;
            }
            resp.Status = 500;
            resp.Message = erro;
            return Json(resp);
        }
    }
}