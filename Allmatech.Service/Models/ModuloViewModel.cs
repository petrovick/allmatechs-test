﻿using Allmatech.Business.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allmatech.Service.Models
{
    public class ModuloViewModel
    {
        public IList<Modulo> Modulos { get; set; }
        public int IdUsuario { get; set; }
    }
}