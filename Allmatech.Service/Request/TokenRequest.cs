﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allmatech.Service.Request
{
    public class TokenRequest
    {
        public int IdModulo { get; set; }
        public string DataHora { get; set; }
    }
}