﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allmatech.Service.Helper
{
    public class ConstantHelper
    {
        public static string ServerUrlAutenticacao()
        {
            return System.Configuration.ConfigurationManager.AppSettings["ServerUrlAutenticacao"];
        }
    }
}