﻿using Newtonsoft.Json;
using System;
using Allmatech.Service.Request;
using System.Web;

namespace Allmatech.Service.Helper
{
    public class ValidarToken
    {
        /*
        public string ValidandoToken(string token)
        {
            string erro = "";
            try
            {
                var jsonToken = RC4.HexToBin(token);
                var json = RC4.Decrypt("", jsonToken, false);
                var tokenRequest = JsonConvert.DeserializeObject<TokenRequest>(json);
                DateTime dataTokenGerado = DateTime.ParseExact(tokenRequest.DataHora, "ddMMyyyy HH:mm", null);
                int idModuloDoSistema = int.Parse(ConstantHelper.IdModulo());
                int segundosDeDiferenca = (int)((dataTokenGerado.TimeOfDay.TotalMilliseconds / 1000 / 60) - ((DateTime.Now.TimeOfDay.TotalMilliseconds) / 1000 / 60));
                if (idModuloDoSistema != tokenRequest.IdModulo)
                {
                    throw new Exception("Token inválido");
                }
                else if (segundosDeDiferenca > 15 || segundosDeDiferenca < -15)
                {//acima de 15 minutos de diferença
                    throw new Exception("horário inválido ou sua sessão expirou.");
                }
                //Session.Add("IdUsuario", tokenRequest);
            }
            catch (Exception ex)
            {
                erro = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
            }
            return erro;
        }
        */
        public CookieRequest ValidarCookieToken(string token)
        {
            string erro = "";
            try
            {

                var jsonToken = RC4.HexToBin(token);
                var json = RC4.Decrypt("", jsonToken, false);
                var cookieRequest = JsonConvert.DeserializeObject< CookieRequest>(json);
                return cookieRequest;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}