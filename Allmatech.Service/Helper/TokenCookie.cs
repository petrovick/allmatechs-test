﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allmatech.Service.Helper
{
    public class GerarTokenCookie
    {
        public string GerarToken(int idUsuario, string senha)
        {

            var jsonToken = new CookieRequest()
            {
                IdUsuario = idUsuario,
                Senha = senha
            };
            var token = RC4.Encrypt("", JsonConvert.SerializeObject(jsonToken), false);
            return RC4.Bin2Hex(token);
        }
    }
}