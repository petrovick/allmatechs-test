﻿using Azul.Generic;
using Azul.Helper;
using Azul.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Azul.Controllers
{
    [ActionFilterSecurity]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            ViewBag.Title = "Home Page";

            int idUsuario = int.Parse(Session["IdUsuario"].ToString());

            var url = ConstantHelper.ServerUrlAutenticacao() + "api/Usuario/BuscarUsuario?idUsuario=" + idUsuario;
            var response = new GenericRequest<Int32, UsuarioResponse>().MakeGetRequest(url);

            ViewBag.NomeUsuario = response.Nome;

            return View();
        }
    }
}
