﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Azul.Helper
{
    public class TokenCookie
    {
        public CookieRequest DescriptografarToken(string token)
        {

            var tokenBin = RC4.HexToBin(token);
            var content = RC4.Decrypt("", tokenBin, false);
            CookieRequest cookie = JsonConvert.DeserializeObject<CookieRequest>(content);
            return cookie;
        }
    }
}