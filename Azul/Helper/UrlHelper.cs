﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Azul.Helper
{
    public class UrlHelper
    {
        public static string IdModulo()
        {
            return System.Configuration.ConfigurationManager.AppSettings["IdModulo"];
        }

        public static string ServerUrlAutenticacao()
        {
            return System.Configuration.ConfigurationManager.AppSettings["ServerUrlAutenticacao"];
        }
    }
}