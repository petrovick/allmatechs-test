﻿using Azul.Request;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Azul.Helper
{
    public class ValidarToken
    {
        public CookieRequest ValidarCookieToken(string token)
        {
            string erro = "";
            try
            {
                var jsonToken = RC4.HexToBin(token);
                var json = RC4.Decrypt("", jsonToken, false);
                var cookieRequest = JsonConvert.DeserializeObject<CookieRequest>(json);
                return cookieRequest;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}