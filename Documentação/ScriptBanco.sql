
create database allmatech;
go
use allmatech;
go
create table Modulo (
	IdModulo int not null identity(1,1),
	Nome varchar(30) not null,
	Descricao varchar(255) not null,
	constraint PKIdModulo primary key(IdModulo)
);
go

create table Usuario (
	IdUsuario int not null identity(1,1),
	Username varchar(30) null,
	Nome varchar(50) null,
	Senha varchar(100) null,
	Facebook bit not null default(0),
	constraint PKIdUsuario primary key(IdUsuario)
);
go
create table UsuarioModulo (
	IdModulo int not null,
	IdUsuario int not null,
	constraint PKUsuarioModulo primary key(IdModulo, IdUsuario)
);
go

alter table Modulo
add Url varchar(255) null;
go

alter table Modulo
add UrlImagem varchar(255) null;
go

alter table Usuario
add RedeSocialId varchar(100) null
go

alter table Usuario
add Token varchar(max) null

insert into Usuario values('user','Gabriel Petrovick', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 0, '', null)

insert into Modulo values('Azul', 'M�dulo da cor Azul', 'http://localhost/Azul/Login/UserToken?token=', 'http://localhost/Allmatech/Content/img/azul.png');
insert into Modulo values('Vermelho', 'M�dulo da cor Vermelha', 'http://localhost/Vermelho/Login/UserToken?token=','http://localhost/Allmatech/Content/img/vermelho.jpg');
insert into UsuarioModulo values(1, 1), (2, 1)

select * from Usuario;

update Modulo set UrlImagem = 'http://us.123rf.com/450wm/krisdayod/krisdayod1209/krisdayod120900151/15248422-textura-de-capacho-de-pl%C3%A1stico-vermelho.jpg' where IdModulo = 2