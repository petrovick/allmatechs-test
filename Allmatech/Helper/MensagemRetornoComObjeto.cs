﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allmatech.Helper
{
    public class MensagemRetornoComObjeto<T> : MensagemRetorno
    {
        public MensagemRetornoComObjeto(string tipoMensagem)
            : base(tipoMensagem)
        {
        }
        public T objetoRetorno { get; set; }

    }
}
