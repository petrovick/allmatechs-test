﻿using System.Linq;

using System.Web;
using System.Web.Mvc;
namespace Allmatech.Helper
{
    using System;
    using System.Collections.Generic;
    public class ActionFilterSecurity : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string cabecalhoAuthorization = "";
            string tokenRecebido = "";
            try
            {
                cabecalhoAuthorization = filterContext.HttpContext.Request.Headers.GetValues("Cookie")[0];
                tokenRecebido = cabecalhoAuthorization.Substring(cabecalhoAuthorization.IndexOf("AllmatechCookie"));//.Where( x => x == "Cookie");
            }
            catch (Exception)
            {
                tokenRecebido = "";
            }
            if (string.IsNullOrEmpty(tokenRecebido) || tokenRecebido.Length <= 16 || tokenRecebido[15] != '=')
            {
                RedirecionarNaoAutenticado(filterContext);
            }
            else
            {
                var idUsuario = new ValidarToken().ValidarCookieToken(tokenRecebido.Split(new char[] { '=' })[1]).IdUsuario;
                filterContext.HttpContext.Session.Add("IdUsuario", idUsuario);
            }
        }

        private void RedirecionarNaoAutenticado(ActionExecutingContext filterContext)
        {
            filterContext.HttpContext.Session.Clear();
            var serverLoginUrl = ConstantHelper.ServerUrl() + "Login/Index";
            filterContext.Result = new RedirectResult(serverLoginUrl);
        }
    }
}