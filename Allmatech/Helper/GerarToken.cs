﻿using Allmatech.Request;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allmatech.Helper
{
    public class GerarToken
    {
        public string Token(int idUsuario, int idModulo)
        {
            var jsonToken = new TokenRequest()
            {
                IdUsuario = idUsuario,
                IdModulo = idModulo,
                DataHora = DateTime.Now.ToString("ddMMyyyy HH:mm")
            };
            var token = RC4.Encrypt("", JsonConvert.SerializeObject(jsonToken), false);
            return RC4.Bin2Hex(token);
        }
    }
}