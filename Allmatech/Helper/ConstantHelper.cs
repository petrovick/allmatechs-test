﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allmatech.Helper
{
    public class ConstantHelper
    {
        public static string ServerUrl()
        {
            return System.Configuration.ConfigurationManager.AppSettings["ServerUrl"];
        }
        public static string FacebookClientId()
        {
            return System.Configuration.ConfigurationManager.AppSettings["FacebookClientId"];
        }
        public static string ClientSecret()
        {
            return System.Configuration.ConfigurationManager.AppSettings["ClientSecret"];
        }
        public static string ServerUrlAutenticacao()
        {
            return System.Configuration.ConfigurationManager.AppSettings["ServerUrlAutenticacao"];
        }
    }
}