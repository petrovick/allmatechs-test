﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allmatech.Helper
{
    public class MensagemHelperComObjeto<T> : MensagemHelper
    {

        public static MensagemRetornoComObjeto<T> MontarMensagemRetorno(List<String> erros, T objetoRetorno, string mensagem)
        {
            MensagemRetornoComObjeto<T> mensagemRetorno;

            if (erros == null || erros.All(x => string.IsNullOrWhiteSpace(x)))
            {
                mensagemRetorno = new MensagemRetornoComObjeto<T>("success");
                mensagemRetorno.Mensagens.Add(string.IsNullOrEmpty(mensagem) ? new Mensagem("Salvo.") : new Mensagem(mensagem));
                mensagemRetorno.objetoRetorno = objetoRetorno;
                return mensagemRetorno;
            }

            else
            {
                mensagemRetorno = new MensagemRetornoComObjeto<T>("error");

                foreach (var erro in erros)
                {
                    if (!string.IsNullOrWhiteSpace(erro))
                        mensagemRetorno.Mensagens.Add(new Mensagem(erro));
                }

                return mensagemRetorno;
            }
        }

        public static MensagemRetornoComObjeto<T> MontarMensagemRetornoSuccess(List<String> success, T objetoRetorno)
        {
            MensagemRetornoComObjeto<T> mensagemRetorno = null;

            if (success != null && !success.All(x => string.IsNullOrWhiteSpace(x)))
            {
                mensagemRetorno = new MensagemRetornoComObjeto<T>("success");

                foreach (var mens in success)
                {
                    mensagemRetorno.Mensagens.Add(new Mensagem(mens));
                }
                mensagemRetorno.objetoRetorno = objetoRetorno;
            }

            else
            {
                mensagemRetorno = new MensagemRetornoComObjeto<T>("success");
            }

            return mensagemRetorno;
        }

        public static MensagemRetornoComObjeto<T> MontarMensagemRetornoWarning(List<String> warns, T objetoRetorno)
        {
            MensagemRetornoComObjeto<T> mensagemRetorno = null;

            if (warns != null && !warns.All(x => string.IsNullOrWhiteSpace(x)))
            {
                mensagemRetorno = new MensagemRetornoComObjeto<T>("warning");

                foreach (var warning in warns)
                {
                    mensagemRetorno.Mensagens.Add(new Mensagem(warning));
                }
                mensagemRetorno.objetoRetorno = objetoRetorno;
            }
            else
            {
                mensagemRetorno = new MensagemRetornoComObjeto<T>("success");
            }

            return mensagemRetorno;
        }

    }
}
