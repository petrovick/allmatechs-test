﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allmatech.Helper
{
    public class MensagemRetorno
    {
        public MensagemRetorno(string tipoMensagem)
        {
            this.TipoMensagem = tipoMensagem;
            this.Mensagens = new List<Mensagem>();
        }

        public String TipoMensagem;
        public IList<Mensagem> Mensagens;
    }

    public class Mensagem
    {
        public Mensagem(String Descricao)
        {
            this.Descricao = Descricao;
        }

        public int Codigo;
        public String Descricao;
    }

}