﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allmatech.Helper
{
    public class MensagemHelper
    {
        ///<summary>
        ///<para>Responsável por Montar a Mensagem a partir da lista de Mensagens de Erro passadas como Parâmetro</para>
        ///<para>Deve-se passar a lista de erros para a construção do objeto MensagemRetorno</para>
        ///<para>Retornar um objeto do tipo MensagemRetorno</para>
        ///</summary>
        public static MensagemRetorno MontarMensagemRetorno(List<String> erros)
        {
            MensagemRetorno mensagemRetorno;

            if (erros == null || erros.All(x => string.IsNullOrWhiteSpace(x)))
            {
                mensagemRetorno = new MensagemRetorno("success");
                mensagemRetorno.Mensagens.Add(new Mensagem("Salvo."));
                return mensagemRetorno;
            }

            else
            {
                mensagemRetorno = new MensagemRetorno("error");

                foreach (var erro in erros)
                {
                    if (!string.IsNullOrWhiteSpace(erro))
                        mensagemRetorno.Mensagens.Add(new Mensagem(erro));
                }

                return mensagemRetorno;
            }
        }

        ///<summary>
        ///<para>Responsável por Montar a Mensagem a partir da lista de Mensagens de Erro passadas como Parâmetro</para>
        ///<para>Deve-se passar a lista de avisos para a construção do objeto MensagemRetorno</para>
        ///<para>Retornar um objeto do tipo MensagemRetorno</para>
        ///</summary>
        public static MensagemRetorno MontarMensagemRetornoWarning(List<String> warns)
        {
            MensagemRetorno mensagemRetorno = null;

            if (warns != null && !warns.All(x => string.IsNullOrWhiteSpace(x)))
            {
                mensagemRetorno = new MensagemRetorno("warning");

                foreach (var warning in warns)
                {
                    mensagemRetorno.Mensagens.Add(new Mensagem(warning));
                }
            }

            else
            {
                mensagemRetorno = new MensagemRetorno("success");
            }

            return mensagemRetorno;
        }

        ///<summary>
        ///<para>Responsável por Montar a Mensagem a partir da lista de Mensagens de Erro passadas como Parâmetro</para>
        ///<para>Deve-se passar a lista de avisos para a construção do objeto MensagemRetorno</para>
        ///<para>Retornar um objeto do tipo MensagemRetorno</para>
        ///</summary>
        public static MensagemRetorno MontarMensagemRetornoSuccess(List<String> success)
        {
            MensagemRetorno mensagemRetorno = null;

            if (success != null && !success.All(x => string.IsNullOrWhiteSpace(x)))
            {
                mensagemRetorno = new MensagemRetorno("success");

                foreach (var mens in success)
                {
                    mensagemRetorno.Mensagens.Add(new Mensagem(mens));
                }
            }

            else
            {
                mensagemRetorno = new MensagemRetorno("success");
            }

            return mensagemRetorno;
        }
    }
}
