﻿using Allmatech.Generic;
using Allmatech.Helper;
using Allmatech.Request;
using Allmatech.Response;
using Facebook;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Allmatech.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            ViewBag.UrlFb = GetFacebookLoginUrl();
            return View();
        }

        [HttpPost]
        public ActionResult Login(UsuarioRequest request)
        {
            string erro = "";
            try
            {

                var url = ConstantHelper.ServerUrlAutenticacao() + "api/Login/Login";
                var loginResponse = new GenericRequest<UsuarioRequest, LoginResponse>().MakePostRequest(request, url);

                erro = loginResponse.Message;
                if (loginResponse.Status == 200)
                {
                    var allmatechCookie = new HttpCookie("AllmatechCookie", loginResponse.AllmatechCookie);
                    allmatechCookie.Expires.AddYears(15);
                    Response.Cookies.Set(allmatechCookie);
                    Session.Add("IdUsuario", loginResponse.IdUsuario);
                    return RedirectToAction("Index", "Home");
                }

                
            }
            catch (Exception ex)
            {
                erro = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
            }
            ViewBag.Erro = erro;
            ViewBag.UrlFb = GetFacebookLoginUrl();

            return View("Index");
            //return RedirectToActionPermanent("Index", "Home");
        }

        public ActionResult Logoff()
        {
            Session.Clear();
            if (Request.Cookies["AllmatechCookie"] != null)
            {
                Response.Cookies["AllmatechCookie"].Expires = DateTime.Now.AddDays(-1);
            }
            return RedirectToAction("Index", "Login");
        }

        public ActionResult Editar()
        {
            var a = Session["IdUsuario"].ToString();
            var idUsuario = int.Parse(Session["IdUsuario"].ToString());
            var url = ConstantHelper.ServerUrlAutenticacao() + "api/Usuario/BuscarUsuario?idUsuario=" + idUsuario;
            UsuarioResponse usuarioResponse = new GenericRequest<UsuarioRequest, UsuarioResponse>().MakeGetRequest(url);
            var model = new UsuarioRequest()
            {
                IdUsuario = usuarioResponse.IdUsuario,
                Facebook = usuarioResponse.Facebook,
                Nome = usuarioResponse.Nome,
                Username = usuarioResponse.Username
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Editar(UsuarioRequest usuario)
        {
            string erro = "";
            MensagemRetornoComObjeto<UsuarioRequest> retorno = null;
            try
            {

                usuario.Senha = Criptografia.sha256(usuario.Senha);
                var url = ConstantHelper.ServerUrlAutenticacao() + "api/Usuario/SalvarUsuario";
                var usuarioResponse = new GenericRequest<UsuarioRequest, UsuarioResponse>().MakePostRequest(usuario, url);
                retorno = MensagemHelperComObjeto<UsuarioRequest>.MontarMensagemRetornoSuccess(new List<string>() { string.IsNullOrEmpty(erro) ? "Salvo com sucesso" : erro }, usuario);
            }
            catch (Exception ex)
            {
                erro += ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                retorno = MensagemHelperComObjeto<UsuarioRequest>.MontarMensagemRetornoSuccess(new List<string>() { erro }, usuario);
            }
            return Json(retorno, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Registrar()
        {
            UsuarioRequest usuario = new UsuarioRequest();
            return View("Editar", usuario);
        }


        public string GetFacebookLoginUrl()
        {
            dynamic parameters = new ExpandoObject();
            parameters.client_id = Helper.ConstantHelper.FacebookClientId();//"1952780664949441";
            parameters.redirect_uri = Helper.ConstantHelper.ServerUrl() + "Login/Retornofb";
            parameters.response_type = "code";
            parameters.display = "page";
            parameters.scope = "email";

            var extendedPermissions = "user_about_me";
            parameters.scope = extendedPermissions;

            var _fb = new FacebookClient();
            var url = _fb.GetLoginUrl(parameters);

            return url.ToString();
        }
        public ActionResult RetornoFb()
        {
            var _fb = new FacebookClient();
            FacebookOAuthResult oauthResult;

            _fb.TryParseOAuthCallbackUrl(Request.Url, out oauthResult);

            if (oauthResult.IsSuccess)
            {
                //Pega o Access Token "permanente"
                dynamic parameters = new ExpandoObject();
                parameters.client_id = Helper.ConstantHelper.FacebookClientId();// "1952780664949441";
                parameters.redirect_uri = Helper.ConstantHelper.ServerUrl() + "Login/Retornofb";
                parameters.client_secret = Helper.ConstantHelper.ClientSecret();
                parameters.code = oauthResult.Code;

                dynamic result = _fb.Get("/oauth/access_token", parameters);

                var accessToken = result.access_token;

                var facebookClient = new FacebookClient(accessToken);
                var me = facebookClient.Get("me?fields=id,name,email") as JsonObject;
                var facebookName = me["name"];
                var facebookId= me["id"].ToString();

                //TODO: Guardar no banco
                var url = ConstantHelper.ServerUrlAutenticacao() + "api/Usuario/BuscarUsuarioPorRedeSocial?redeSocialId=" + facebookId;
                var usuarioResponse = new GenericRequest<UsuarioRequest, UsuarioResponse>().MakeGetRequest(url);

                if (usuarioResponse.Status == 0)
                {
                    ViewBag.Erro = usuarioResponse.Message;
                    return View("Index");
                }
                else
                {
                    var usuarioRequest = new UsuarioRequest();
                    usuarioRequest.Token = accessToken;
                    usuarioRequest.Facebook = usuarioResponse.Facebook;
                    usuarioRequest.IdUsuario = usuarioResponse.IdUsuario;
                    usuarioRequest.Nome = facebookName + "";
                    usuarioRequest.RedeSocialId = facebookId;
                    url = ConstantHelper.ServerUrlAutenticacao() + "api/Usuario/SalvarUsuario";
                    usuarioResponse = new GenericRequest<UsuarioRequest, UsuarioResponse>().MakePostRequest(usuarioRequest, url);
                }

                url = ConstantHelper.ServerUrlAutenticacao() + "api/Facebook/Login";
                UsuarioRequest request = new UsuarioRequest()
                {
                    RedeSocialId = facebookId,
                    Token = accessToken
                };
                var loginResponse = new GenericRequest<UsuarioRequest, LoginResponse>().MakePostRequest(request, url);
                
                if (loginResponse.Status == 200)
                {
                    var allmatechCookie = new HttpCookie("AllmatechCookie", loginResponse.AllmatechCookie);
                    allmatechCookie.Expires.AddYears(15);
                    Response.Cookies.Set(allmatechCookie);
                    Session.Add("IdUsuario", loginResponse.IdUsuario);
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return RedirectToAction("Index", "Home");
        }
    }
}