﻿using Allmatech.Generic;
using Allmatech.Helper;
using Allmatech.Models;
using Allmatech.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Allmatech.Controllers
{
    [ActionFilterSecurity]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult Modulos()
        {
            int idUsuario = int.Parse(Session["IdUsuario"].ToString());
            var url = ConstantHelper.ServerUrlAutenticacao() + "api/Modulo/ListarModulosDoUsuario?idUsuario=" + idUsuario;
            var moduloViewModel = new GenericRequest<int, ModuloViewModel>().MakeGetRequest(url);
            return View(moduloViewModel);
        }

    }
}
