﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allmatech.Response
{
    public class LoginResponse : BaseResponse
    {
        public string AllmatechCookie { get; set; }
        public int IdUsuario { get; set; }

    }
}