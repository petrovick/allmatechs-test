﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allmatech.Response
{
    public class BaseResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }
    }
}