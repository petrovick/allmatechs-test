﻿using Allmatech.ViewModel.ObjectModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allmatech.ViewModel
{
    public class UsuarioModuloViewModel
    {
        public Modulo Modulo { get; set; }
        public string TokenAcesso { get; set; }
        
    }
}