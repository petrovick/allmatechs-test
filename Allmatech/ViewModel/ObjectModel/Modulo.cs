﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allmatech.ViewModel.ObjectModel
{
    public class Modulo
    {
        public Int32 IdModulo { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Url { get; set; }
        public string UrlImagem { get; set; }
    }
}