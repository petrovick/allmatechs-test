﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allmatech.Request
{
    public class UsuarioRequest
    {
        public Int32 IdUsuario { get; set; }
        public string Nome { get; set; }
        public string Username { get; set; }
        public string Senha { get; set; }
        public Boolean Facebook { get; set; }
        public string Token { get; set; }
        public string RedeSocialId { get; set; }
    }
}