﻿using Allmatech.Business.BusinessObject;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Xml;

//using System.Data.EntityClient;
//using System.Data.Objects;

namespace Allmatech.Model.DataContext
{
    public partial class AllmatechEntities : DbContext
    {
        #region Constructors
        public AllmatechEntities()
            : base("AllmatechEntities")
        {
            Database.SetInitializer<AllmatechEntities>(null);
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 180;
        }
        #endregion

        public virtual IDbSet<Usuario> Usuario { get; set; }
        public virtual IDbSet<Modulo> Modulo { get; set; }
        public virtual IDbSet<UsuarioModulo> UsuarioModulo { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Remover pluralização do nome das tabelas
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Usuario>()
                .ToTable("Usuario");

            modelBuilder.Entity<Modulo>()
                .ToTable("Modulo");

            modelBuilder.Entity<UsuarioModulo>()
                .ToTable("UsuarioModulo");
        }
        
    }
}
