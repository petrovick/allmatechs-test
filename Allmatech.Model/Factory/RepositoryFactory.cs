﻿using Allmatech.Model.GenericRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allmatech.Model.Factory
{
    public class RepositoryFactory<TEntity> where TEntity : class
    {
        private static RepositoryFactory<TEntity> _repositoryFactory;

        private RepositoryFactory()
        {
        }

        public static RepositoryFactory<TEntity> GetInstance()
        {
            return _repositoryFactory ?? (_repositoryFactory = new RepositoryFactory<TEntity>());
        }

        public IRepository<TEntity> GetRepository()
        {
            return new Repository<TEntity>();
        }
    }
}